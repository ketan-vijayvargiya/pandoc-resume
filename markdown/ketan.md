Ketan Vijayvargiya
===

---

> Principal Engineer, Amazon Web Services

> <hi@ketanvijayvargiya.com> • (206) 334-9803 • Bothell, WA

---

Experience
---

Amazon Web Services

:   **07/2019 – Present: Principal Engineer, Artificial Intelligence and Machine Learning (Bellevue, US)**

Technical leader in the [SageMaker](https://aws.amazon.com/sagemaker/) organization. Develop fully-managed integrated development environments (IDEs) which empower customers, both individuals and enterprise teams, to perform end-to-end AI and analytics workloads in the cloud.

Key focus areas:

- **Compute platform:** Tech lead for infrastructure that powers millions of container-based applications per-month for multiple SageMaker products. Focus on both feature additions for a wide variety of enterprise use cases and long-term architectural investments. Example of the former: enabled customers to securely leverage Docker within their containers without using privileged containers on the platform. Example of the latter: drove leadership alignment and executed a backend re-architecture of [SageMaker Notebooks](https://aws.amazon.com/sagemaker/notebooks/), a seven year old product, to set it up for success over the next decade. Also, app startup latency reduction and cellularization.
- **Unified runtime for AI and data:** Presented a bold vision to move away from single-framework runtimes to a unified runtime that contains *all packages users may need* and *works the same across all product lines*. Addressed concerns regarding technical feasibility and operational maintenance, successfully pitched the idea for [open-sourcing](https://github.com/aws/sagemaker-distribution), and secured alignment with leadership across multiple organizations. Currently leading a multi-year vision for adoption across these organizations. Additionally, delivered a talk at [Apache's Community Over Code conference](https://ketanvijayvargiya.com/3-unified-ai-ml-runtime-how-sagemaker-distribution-aims-to-radically-simplify-ml-development/).
- **Product engineering:** Tech lead or reviewer for several product features. Example: addressed the complexity of AWS’s [Identity and Access Management (IAM)](https://aws.amazon.com/iam/) for enterprise customers by promoting alternatives such as [IAM Identity Center](https://aws.amazon.com/iam/identity-center/)'s [trusted identity propagation](https://docs.aws.amazon.com/singlesignon/latest/userguide/trustedidentitypropagation.html) and OIDC. Moreover, a key contributor to API design and security of multiple SageMaker products.

Earlier role: tech lead for [Lookout for Metrics](https://aws.amazon.com/lookout-for-metrics/), an AI-based anomaly detection and root cause analysis service for time-series data. Ideated, developed and launched the product from the ground up.

Amazon

:   **08/2013 – 06/2019: Software Engineer**

Technical leader & software engineer across various organizations: [Lambda](https://aws.amazon.com/lambda/), AWS's serverless compute service; automated marketing across social, e-mail and text channels; competitive monitoring.

Earlier

:   **08/2010 – 07/2013**: Software Engineer in Adobe and Snapdeal.

Education
---

2006 - 2010

:   **B.S. in Computer Science, Delhi College of Engineering**
